# sysadmin-public
OpenSwirl Debian GNU/Linux System Administrator Configuration.
Based on Debian GNU/Linux Buster (current testing) for single host.

## What To Do After A Fresh Installation?

### Modify Repositories
*   Define your debian version in `/etc/apt/apt.conf`.
*   Leave `/etc/apt/sources.list` empty, then put on `/etc/apt/sources.list.d/` for each repo. Filename format: `[repo].[codename].list`.
*   Disable translation. Delete any downloaded translation files.

    ```console
    # rm /var/lib/apt/lists/*-en
    ```
*   Put `99translations` file on `/etc/apt/apt.conf.d`.
*   Update and dist-upgrade.

    ```console
    # apt update && apt dist-upgrade
    ```

### Firmware
*   Check if all hardware is recognized.
*   Install necessary firmware.

    ```console
    # apt install firmware-linux firmware-linux-nonfree
    ```
*   Pin/name hardware (NIC, USB device, etc) in _udev_ if necessary.

### Text Editor
*   Personally, I prefer vim-nox over vim-tiny. 
*   Remove the default vim-tiny.

    ```console
    # apt --purge remove vim-tiny
    ```
*   Install vim-nox.

    ```console
    # apt install vim-nox
    ````
*   Edit `vimrc` and/or `vimrc.local` to your needs.

### Remove/Disable Unwanted Service
*   See installed and running services.

    ```console
    # ps aux
    # netstat -nptlu
    # dpkg -l
    ```
*   Disable _exim4_ (and uninstall).

    ```console
    # systemctl stop exim4
    # apt --purge remove exim4
    # apt autoremove
    ```
*   Disable _rpcbind_.

    ```console
    # systemctl stop rpcbind
    # systemctl disable rpcbind
    ```

### Update Kernel Parameters
*   Create a file in `/etc/sysctl.d/`.
*   Edit `vm.swappiness` to 1 for minimize swapping. Important on SSD disk.
*   Edit `net.ipv4.ip_local_port_range` if you will have many outgoing connections. 
*   Execute.

    ```console
    # sysctl -p
    ```

### Install Necessary Packages
*   Server monitoring: _sysstat_, _htop_, _iftop_.

    ```console
    # apt install sysstat htop iftop
    ```
*   Web-related tools: _elinks_, _curl_, _dig_.

    ```console
    # apt install elinks curl dnsutils
    ```
*   Various.

    ```console
    # apt install build-essential
    ```

### Modify Skeleton Directory
*   Change `.bashrc` to enable color command prompt.
*   Add custom aliases.

### SSH Banner
*   Create `/etc/ssh-banner` file.
*   Update `Banner` directive in `/etc/ssh/sshd_config`.

### SSH Key
*   Copy your ssh key to `~/.ssh/authorized_keys`, or using `ssh-copy-id` command.

### Monitoring
Create a monitoring user.

## Firewall
For a single host inside a secure network (private IP address), I would recommend using _ufw_. _Fail2ban_ is necessary if you forward a port from the outside.

## Web Server

Rules of thumb:
*   Enable modules you want to use, and disable them if you don't.
*   Try not to edit base configuration files (e.g. `nginx.conf`, `apache.conf`) because apt upgrade might replace them.
*   If you are using _cacti_, _phpmyadmin_, _zabbix_ or any well-known tools, change the aliases. Put extra security (ssl, auth, restrict ip address) if necessary.
*   Add `X-Forwarded-For` in logging if necessary.
*   Put your vhost configuration in seperate files in `sites-available` directory.
*   If possible, turn off server tokens.

### Apache
After installation:
*   Add `ServerName` directive in `conf-available/fqdn.conf`, and enable it using `a2enconf fqdn`.
*   Copy and edit `security.conf`, then enable it.
*   Enable modules you want to use, and disable them if you don't.
*   If you want to compress json, add this line in `/etc/apache/mods-available/deflate.conf`:

    ```config
    AddOutputFilterByType DEFLATE application/json
    ```
*   Enable `status` module if you want to monitor your webserver via _cacti_.
*   Disable `serve-cgi-bin` configuration if you don't know what it is.
*   Adjust `MaxRequestWorkers` on `mpm_prefork.conf` to your needs (if you are using _PHP_ with this apache module).
*   If you are using _cacti_ or _phpmyadmin_, add extra security by adding Auth-Basic authentification. 

### Lighttpd
*Not today*

### Nginx
*   Use ```nginx -t``` to check your edited configuration files.
*   There are no `nginxensite` or `nginxdissite`, so you have to link your vhost configuration files manually.

## MySQL Database
Using Percona MySQL DB:

1.  Add repo to `/etc/apt/sources.list.d/` (stable release).

    ```
    deb http://repo.percona.com/apt stretch main
    ```
2.  Add percona key.

    ```console
    # apt-key adv --keyserver keys.gnupg.net --recv-keys 8507EFA5
    ```
3.  Install Percona Server MySQL and follow installation.

    ```console
    # apt update
    # apt install percona-server-server-5.7
    ```
4.  Make sure it reads percona config file.

    ```console
    # update-alternatives --list my.cnf
    /etc/mysql/percona-server.cnf
    ```
5.  Config in `mysqld.cnf`
    *   `innodb-file-per-table` (default in 5.7).
    *   `open-files-limit` (see #6 below).
    *   `innodb_thread_concurrency` is 2 x CPU cores.
    *   Set `innodb-buffer-pool-size` to 75% of server RAM.
    *   Disable slow query log.
    *   `log_timestamps = system` to show time in your current server timezone.
6.  Update systemd (under _[Service]_)

    ```
    LimitNOFILE=102400
    LimitNPROC=102400
    ```
7.  Don't forget to reload systemd.

    ```console
    # systemctl daemon-reload
    ```
8.  Create `client.cnf`, and update its permission to 640.

    ```console
    # chmod 640 /etc/mysql/percona-server.conf.d/client.cnf
    # chown root.mysql /etc/mysql/percona-server.conf.d/client.cnf
    ```
9.  Restart MySQL, and remove test database.

    ```console
    # systemctl restart mysql
    # mysql_secure_installation
    ```
10. Add a monitoring user.


## Server Monitoring

### Cacti
1.  Prerequisites: 
    *   Web server with _PHP_ (for frontend).
    *   Database (_MySQL_ DB in this case).
2.  Apt.

    ```console
    # apt install cacti
    ```
3.  For extra security, add http auth basic and/or restricts only for specific IP addresses.
4.  Use percona cacti template. Add percona repo if you haven't.

    ```console
    # apt install percona-cacti-templates
    ```
5.  Import templates you want to use.

    ```console
    # cd /usr/share/cacti/resource/percona/templates
    # ls -1
    cacti_host_template_percona_apache_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_galera_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_gnu_linux_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_jmx_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_memcached_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_mongodb_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_mysql_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_nginx_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_openvz_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_rds_server_ht_0.8.6i-sver1.1.7.xml
    cacti_host_template_percona_redis_server_ht_0.8.6i-sver1.1.7.xml
    # php /usr/share/cacti/cli/import_template.php \
    --filename=cacti_host_template_percona_gnu_linux_server_ht_0.8.6i-sver1.1.7.xml \
    --with-user-rras='1:2:3:4'
    # php /usr/share/cacti/cli/import_template.php \
    --filename=cacti_host_template_percona_mysql_server_ht_0.8.6i-sver1.1.7.xml \
    --with-user-rras='1:2:3:4'
    # php /usr/share/cacti/cli/import_template.php \
    --filename= cacti_host_template_percona_apache_server_ht_0.8.6i-sver1.1.7.xml \
    --with-user-rras='1:2:3:4'
    ```
6.  Linux server template:
    *   Copy monitoring user rsa-key (`id_rsa` and `id_rsa.pub`) to `/etc/cacti`. Make sure _id_rsa_ file only root and poller (cacti user) have read access.
    *   Copy configuration from `/usr/share/cacti/site/scripts/ss_get_by_ssh.php` to `/etc/cacti/ss_get_by_ssh.php.cnf` and modify to your needs (only the `CONFIGURATION` part, delete the reset).
    *   Add device and graph from frontend.
7.  MySQL server template:
    *   Copy configuration from `/usr/share/cacti/site/scripts/ss_get_mysql_stats.php` to `/etc/cacti/ss_get_mysql_stats.php.cnf` and modify to your needs (only the `CONFIGURATION   part, delete the reset).
    *   Add device and graph from frontend.
8.  Apache server template:
    *   Enable Apache `mod-status` only allowed for cacti poller IP addresses.
    *   Edit `/etc/cacti/ss_get_by_ssh.php.cnf` to your needs.
    *   Add device and graph from frontend.
9.  Further reads about [percona cacti templates](https://www.percona.com/doc/percona-monitoring-plugins/LATEST/cacti/index.html).


### Zabbix
1.  Prerequisites: 
    *   web server with _PHP_ (for frontend).
    *   database (_MySQL_ in this case).
   
2.  Add zabbix repo.

    ```console
    # echo 'deb http://repo.zabbix.com/zabbix/3.4/debian stretch main' > /etc/apt/sources.list.d/stretch.zabbix.list
    # apt-key adv --keyserver keys.gnupg.net --recv-keys A14FE591
    # apt update
    ```
3.  Install zabbix (with _MySQL_ as its database) and _php_ frontend.

    ```console
    # apt install zabbix-server-mysql zabbix-frontend-php
    ```
4.  Create zabbix database user and database, then import zabbix initial database.

    ```console
    $ mysql -u root -p
    Enter password:
    mysql> CREATE USER 'zabbix'@'%' IDENTIFIED WITH mysql_native_password AS '***';
    Query OK, 0 rows affected (0.00 sec)
    
    mysql> GRANT USAGE ON *.* TO 'zabbix'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    Query OK, 0 rows affected (0.00 sec)
    
    mysql> CREATE DATABASE IF NOT EXISTS `zabbix`;
    Query OK, 1 row affected (0.01 sec)
    
    mysql> GRANT ALL PRIVILEGES ON `zabbix`.* TO 'zabbix'@'%';
    Query OK, 0 rows affected (0.00 sec)
    
    mysql> \q
    
    $ cp -p /usr/share/doc/zabbix-server-mysql/create.sql.gz /tmp
    $ cd /tmp
    $ gunzip create.sql.gz
    $ mysql -u zabbix -p zabbix < /tmp/create.sql
    Enter password:
    $
    ```
5.  Update config to your needs.
6.  For extra security, add http auth basic and/or restricts only for specific IP addresses.

## Backup
*Not today*
