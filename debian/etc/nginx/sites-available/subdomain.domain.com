# Replace subdomain.domain.tld with your (sub-)domain

server {
  listen 80;

  root /var/www/subdomain.domain.tld;
  index index.html index.htm;

  server_name subdomain.domain.tld;

  # Redirect all http to https
  return 301 https://$server_name$request_uri;

  location / {
    try_files $uri $uri/ =404;
  }

}

server {
  listen 443 ssl;
  server_name subdomain.domain.tld;

  # HSTS header
  add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

  access_log "/var/log/nginx/subdomain.domain.tld.access.log" combined;
  error_log "/var/log/nginx/subdomain.domain.tld.error.log" info;

  # Put your ssl certificate here
  ssl_certificate "/etc/letsencrypt/live/subdomain.domain.tld/fullchain.pem";
  ssl_certificate_key "/etc/letsencrypt/live/subdomain.domain.tld/privkey.pem";
  ssl_trusted_certificate "/etc/letsencrypt/live/subdomain.domain.tld/chain.pem";
  ssl_dhparam "/etc/nginx/dhparam.pem";

  ssl_session_cache shared:le_nginx_SSL:1m;
  ssl_session_timeout 1440m;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers "ECDHE-ECDSA-AES128-GCM-SHA256 ECDHE-ECDSA-AES256-GCM-SHA384 ECDHE-ECDSA-AES128-SHA ECDHE-ECDSA-AES256-SHA ECDHE-ECDSA-AES128-SHA256 ECDHE-ECDSA-AES256-SHA384 ECDHE-RSA-AES128-GCM-SHA256 ECDHE-RSA-AES256-GCM-SHA384 ECDHE-RSA-AES128-SHA ECDHE-RSA-AES128-SHA256 ECDHE-RSA-AES256-SHA384 DHE-RSA-AES128-GCM-SHA256 DHE-RSA-AES256-GCM-SHA384 DHE-RSA-AES128-SHA DHE-RSA-AES256-SHA DHE-RSA-AES128-SHA256 DHE-RSA-AES256-SHA256 EDH-RSA-DES-CBC3-SHA";

  root /var/www/subdomain.domain.tld;
  index index.html index.htm;

  # Proxy pass for Python websocket server
  location /websocket-path/ {
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";

    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $http_host;
    proxy_redirect off;

    proxy_pass http://127.0.0.1:9000/;
  }

  # If you are using php-fpm
  location ~ \.php$ {
    try_files $uri =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include fastcgi_params;
  }

  # PhpMyAdmin
  location /obviously-not-phpmyadmin-path {
    alias /usr/share/phpmyadmin;

    auth_basic "Restricted Access";
    auth_basic_user_file "/etc/phpmyadmin/admin.htpasswd";

    location ~ ^/obviously-not-phpmyadmin-path/(.*\.php)$ {
      alias /usr/share/phpmyadmin/$1;
      fastcgi_split_path_info ^(.+\.php)(/.+)$;
      fastcgi_pass unix:/run/php/php7.0-fpm.sock;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      include fastcgi_params;
    }

    location ~* ^/obviously-not-phpmyadmin-path/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
      alias /usr/share/phpmyadmin/$1;
    }
  }

  # Static file (e.g.: images)
  location /img {
    alias /var/www/img;
    location ~* ^.+\.(jpeg|gif|png|jpg) {
      add_header Cache-control "public";
      expires 90d;
    }

    try_files $uri $uri/ =404;
  }

  location / {
    try_files $uri $uri/ =404;
  }

}
# vim: et sw=2 ts=2
